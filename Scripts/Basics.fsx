// Function: takes an input and gives an output
let addFive x = x + 5 // Signature: addFive : x:int -> int
(addFive 2)

// Expression: any F# code that produces a type.
// It can be anything. Because almost everything in F# returns a type.
let placeholderText = "elementum"
printfn "%s" placeholderText // Returns unit which is a type that means no actual value

// addFive is pure function that does not have any side-effects.
// Below is a non-pure function interaction.
let mutable placeholderNumber = 1
placeholderNumber <- addFive placeholderNumber // Returns unit
printfn "%d" placeholderNumber

// Immutable values
let placeholderNumber2 = 1
let addOne = placeholderNumber2 + 1

printfn "%d" placeholderNumber2 // placeholderNumber2 Is not updated
placeholderNumber2 = placeholderNumber2 + 2 // Checks equality

// Functions as first-class language constructs
let number = 5 // A function that is named and returns a value

// This is actually a function that returns a function that takes an argument
// and returns by adding 3 to it. If there was another parameter, then it would
// have another nested function. This is why F# function signatures are shown with
// arrow signs for better understanding. This function nesting is called currying.
let addThree x = x + 3 // Reality: addThree = fun x -> x + 3
addThree 5

// Functions can be stored in data structures
let additionFuncs = [ addThree; addFive ]
additionFuncs.[1] 5

// Higher order function that takes a function as an argument
let add op arg = op arg
let add2 (op, arg) = op arg // Restricts currying

printfn "%d" (add addThree 4)
printfn "%d" (add2 (addThree, 4))

// Inline function
printfn "%b" ((fun num -> num % 2 = 0) 5)

// Create new function from existing one
let addThreeToFour () = addThree 4
let addThreeToFourV2 = fun () -> addThree 4 // Another way to declare by returning a function

printfn "%d" (addThreeToFour ())
printfn "%d" (addThreeToFourV2 ())

// Function composition with operators
let square x = x * x
let squareAndAddThree = square >> addThree
let addThreeAndSquare = square << addThree

printfn "%d" (squareAndAddThree 5)
printfn "%d" (addThreeAndSquare 5)