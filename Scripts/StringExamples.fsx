// Assigns a string to a function and marks it as Literal.
// Marking a function as Literal instructs the compiler to
// treat it as a compile-time constant. Compile time constants
// are replace with the actual value by compiler! Named Literals
// should be pascal cased.
[<Literal>]
let HelloWorld = "Hello World!"
printfn "%s" HelloWorld

// Triple quoted strings to quotation marks
let xmlFragment = """<book author="Milton, John" title="Paradise Lost">"""
printfn "%s" xmlFragment

// Multi-line string that is printed without new lines
let multiLineText = "natoque penatibus\
                    et magnis\
                    dis"
printfn "%s" multiLineText

// Indexing
printfn "%c" multiLineText.[2]

// Slicing work by slicing a string with from and to indices inclusively
printfn "%s" multiLineText.[2..4]

// Concatenation
let firstPart = "natoque"
let secondPart = "penatibus"

printfn "%s" (firstPart + secondPart)

// Concat using interpolation
printfn $"{(firstPart + secondPart)}"