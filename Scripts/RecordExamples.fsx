// An immutable record type
type Car =
  { Model: string
    Manufacturer: string
    Year: int }

// A record instance
let firstTeslaS = { Model = "S"; Manufacturer = "Tesla"; Year = 2020 }

// A new instance with different property value
let latestTeslaS = { firstTeslaS with Year = 2021 }

// Pattern matching with records
let checkCarType info =
  match info with
  | { Model = "S"; Manufacturer = "Tesla"; Year = 2021 } -> printfn "Cool car!"
  | { Model = "S"; Manufacturer = "Tesla"; Year = 2020 } -> printfn "Old car!"
  | { Model = model; Manufacturer = manufacturer; Year = year } -> printfn $"Piece of Junk: {manufacturer} {model} from {year}"

checkCarType firstTeslaS
checkCarType latestTeslaS
checkCarType { Model = "Allion"; Manufacturer = "Toyota"; Year = 2008 }

// Anonymous Record
let airbusA380 = {| Model = "A380"; Manufacturer = "Airbus" |}
printfn $"{airbusA380.Model} - {airbusA380.Manufacturer}"
