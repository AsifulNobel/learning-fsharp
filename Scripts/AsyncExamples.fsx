#r "paket: nuget FSharp.Data"

open FSharp.Data

// An asynchonous task definition or async primitive.
// .NET Task and F# Async are conceptually different.
let waitForThreeSeconds =
  async {
    printfn "Sleeping for 1 second"
    do! Async.Sleep(1000)

    let! response = Http.AsyncRequestString("https://httpbin.org/get")
    printfn "%s" response

    printfn "Sleeping for 2 seconds"
    do! Async.Sleep(2000)

    printfn "Finished"
  }

// Runs the Async task asynchronously and does not wait for its result to come
// back. This is like fire and forget. Exception stack trace also does not show
// where the function was called from.
waitForThreeSeconds |> Async.Start
printfn "Started waitForThreeSeconds async"

// Runs the Async task asynchronously and waits for its result to come back.
// The thread is blocked during this execution. Mainly used once in whole app life-cycle.
waitForThreeSeconds |> Async.RunSynchronously
printfn "Started waitForThreeSeconds sync"

// Runs the Async task as a Task and executes on thread pool!
waitForThreeSeconds |> Async.StartAsTask
printfn "Started waitForThreeSeconds task"

// Runs the Async task asynchronously with current thread context, but does not wait
// for its result to come back.
waitForThreeSeconds |> Async.StartImmediate
printfn "Started waitForThreeSeconds immediate"
